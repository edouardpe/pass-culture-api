from recommendations_engine.offers import get_offers_for_recommendations_discovery
from recommendations_engine.recommendations import create_recommendations_for_discovery, \
    give_requested_recommendation_to_user

__all__ = (
    'create_recommendations_for_discovery',
    'get_offers_for_recommendations_discovery',
    'give_requested_recommendation_to_user',
)
