class Offerer(object):
    def __init__(self,
                 id: str = None,
                 siren: str = None):
        self.id = id
        self.siren = siren
