from sandboxes.scripts import sandbox_industrial, \
    sandbox_payment, sandbox_activation, \
    sandbox_allocine, sandbox_beneficiary_import, \
    sandbox_bookings_recap
